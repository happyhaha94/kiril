<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix'=>'admin','namespace'=>'Admin'], function(){
    Route::get('dashboard', 'DashboardController@index');
    Route::get('products/{id}/gallery', 'ProductsController@createGallery')->name('createGallery');
    Route::post('products/{id}/gallery', 'ProductsController@storeGallery')->name('storeGallery');
    Route::resource('products', 'ProductsController');
    Route::get('categories/sort', 'CategoriesController@sort')->name('sort');
    Route::post('categories/sortAjax', 'CategoriesController@sortAjax')->name('categorySortAjax');
    Route::resource('categories', 'CategoriesController');
    Route::resource('slider', 'SliderController');
});