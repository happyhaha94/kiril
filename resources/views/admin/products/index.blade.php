@extends('layouts.app')

@section('content')
 <div class="container">
 	<div class="panel panel-default">
		<div class="panel-body">
			<h3>Продукты</h3>
			<a href="{{route('products.create')}}" class="btn btn-success">Создать</a>
			<table class="table">
				<thead>
				<tr>
					<th>ID</th>
					<th>Название</th>
					<th>Цена</th>
					<th>Категория</th>
					<th>Действия</th>
				</tr>
				</thead>

				<tbody>
				@foreach($products as $product)
					<tr>
						<td>{{$product->id}}</td>
						<td>{{$product->title}}</td>
						<td>{{$product->price}}</td>
						<td>{{$product->getCategory()}}</td>
						<td>
							<a href="{{ route('products.show', $product->id) }}">
								<img src="/img/show.png" alt="" class="edit-icon">
							</a>
							<a href="{{ route('products.edit', $product->id) }}">
								<img src="/img/edit.svg" alt="" class="edit-icon">
							</a>

							{!! Form::open(['method' => 'DELETE',
                                'route' => ['products.destroy', $product->id]]) !!}
							<button class="delete-task" onclick="return confirm('are you sure')">
								<img src="/img/delete.png" alt="" class="delete-icon">
							</button>
							{!! Form::close() !!}

						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection