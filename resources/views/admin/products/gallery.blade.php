@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3>Галерея продукта </h3>

                        {!! Form::open(['route' => ['storeGallery', $product->id],'files'=>true]) !!}
                        <div class="form-group">
                            <div class="col-md-6">
                                <input type="file" name="images[]" class="form-control" multiple>
                            </div>
                            <br> <br>
                            <div class="col-md-12">
                                @foreach($product->images as $image)
                                    <img src="/{{$image}}" alt="" width="200" height="200">
                                @endforeach
                            </div>

                            <div class="col-md-12">
                                <button class="btn btn-success">Добавить</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection