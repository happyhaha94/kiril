@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="panel panel-default">
					<div class="panel-body">
						<a href="{{route('createGallery',['id'=>$product->id])}}" class="btn btn-default">Галерея</a>
						<table class="table">
							<thead>
							<tr>
								<th>Название</th>
								<th>Цена</th>
								<th>Картинка</th>
							</tr>
							</thead>

							<tbody>
							<tr>
								<td>{{$product->title}}</td>
								<td>{{$product->price}}</td>
								<td>
									<img src="/{{$product->getImage()}}" alt="" width="100" height="100">
								</td>
							</tr>
							</tbody>
						</table>
					</div>
				</div>

			</div>
		</div>
	</div>
@endsection