@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="col-md-10 col-md-offset-1">
					<h3>Добавить продукт</h3>

					{!! Form::open(['route' => 'products.store','files'=>true]) !!}
					<div class="form-group">
						<div class="col-md-6">
							<input type="text" class="form-control" name="title" value="" placeholder="Название">
							<br>
							<input type="text" class="form-control" name="price" value="" placeholder="Цена">
							<br>
							{{Form::select('category_id', $categories, null, ['placeholder' => 'Выберите категорию', 'class'=>'form-control'])}}
							<br>
							<input type="file" name="image" class="form-control">
							<br>
						</div>
						
						<div class="col-md-12">
							<textarea name="description" id="" cols="30" rows="10" class="form-control"></textarea>
						</div>



						<div class="col-md-12">
							<button class="btn btn-success">Добавить</button>
						</div>
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection