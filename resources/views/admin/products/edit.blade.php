@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="col-md-10 col-md-offset-1">
						<h3>Добавить продукт</h3>

						{!! Form::open(['route' => ['products.update', $product->id],'files'=>true,'method'=>'put']) !!}
						<div class="form-group">
							<div class="col-md-6">
								<input type="text" class="form-control" name="title" value="{{$product->title}}" placeholder="Название">
								<br>
								<input type="text" class="form-control" name="price" value="{{$product->price}}" placeholder="Цена">
								<br>
								{{Form::select('category_id', $categories, $product->category ? $product->category->id : null, ['placeholder' => 'Выберите категорию', 'class'=>'form-control'])}}
								<br>
								<input type="file" name="image" class="form-control">
								<img src="/{{$product->getImage()}}" alt="" width="200">
								<br>
							</div>

							<div class="col-md-12">
								<textarea name="description" id="" cols="30" rows="10" class="form-control">{{$product->description}}</textarea>
							</div>



							<div class="col-md-12">
								<button class="btn btn-warning">Изменить</button>
							</div>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection