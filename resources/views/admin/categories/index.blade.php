@extends('layouts.app')

@section('content')
 <div class="container">
 	<div class="panel panel-default">
		<div class="panel-body">
			<h3>Категории</h3>
			<a href="{{route('categories.create')}}" class="btn btn-success">Создать</a>
			<a href="{{route('sort')}}" class="btn btn-default">Сортировать</a>
			<table class="table">
				<thead>
				<tr>
					<th>ID</th>
					<th>Название</th>
					<th>Действия</th>
				</tr>
				</thead>

				<tbody>
				@foreach($categories as $task)
					<tr>
						<td>{{$task->id}}</td>
						<td>{{$task->title}}</td>
						<td>
							<a href="{{ route('categories.edit', $task->id) }}">
								<img src="/img/edit.svg" alt="" class="edit-icon">
							</a>

							{!! Form::open(['method' => 'DELETE',
                                'route' => ['categories.destroy', $task->id]]) !!}
							<button class="delete-task" onclick="return confirm('are you sure')">
								<img src="/img/delete.png" alt="" class="delete-icon">
							</button>
							{!! Form::close() !!}

						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection