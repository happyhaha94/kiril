@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="box-header with-border">

        </div>
        <div class="box-body panel panel-default">
            <div class="row panel-body">
                <div class="col-md-3">

                </div>
                <div class="col-md-6">
                    <div style="padding-left:30px">
                        <span style="width:100%" id="save" class="btn btn-success">Сохранить</span>
                    </div>
                    <br>
                    <div id="sortableResult"></div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('customJS')
    <script>
        (function(){
            $.post('<?= route('categorySortAjax'); ?>', {_token: '<?= csrf_token() ?>'}, function(data){
                $('#sortableResult').html(data);
            });


            $('#save').on('click', function(){
                sortable = $('.sortable').nestedSortable('toArray');

                $('#sortableResult').slideUp(function(){
                    $.post('{{ route('categorySortAjax') }}', { sortable: sortable, _token: '<?= csrf_token() ?>'}, function(data){
                        $('.sortableResult').html(data);
                        $('#sortableResult').slideDown();
                    });
                });

            });
        })();
    </script>
@endsection