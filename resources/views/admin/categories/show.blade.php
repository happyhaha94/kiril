@extends('welcome')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<h3> {{$task->title}} </h3>	
			<p>
				{{$task->description}}
			</p>
		</div>
	</div>
</div>
@endsection