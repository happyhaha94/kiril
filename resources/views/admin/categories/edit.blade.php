@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="col-md-10 col-md-offset-1">
						<h3>Изменить {{$category->title}}</h3>

						{!! Form::open(['route' => ['categories.update', $category->id], 'method'=>'put']) !!}
						<div class="form-group">
							<input type="text" class="form-control" name="title" value="{{$category->title}}">
							<br>
							<button class="btn btn-warning">Изменить</button>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection