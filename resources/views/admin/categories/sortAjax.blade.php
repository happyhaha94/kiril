<?php
use App\SortService;

echo SortService::getOl($items);


?>
<script>
    $(document).ready(function(){
        $('.sortable').nestedSortable({
            handle: 'div',
            items: 'li',
            toleranceElement: '> div',
            excludeRoot: true,
        });
    })
</script>
<style>
    .sortable div{
        cursor: all-scroll;
        padding: 10px;
        background: #e2e2e2;
        color: #404040;
        margin-bottom: 8px;
    }
</style>
