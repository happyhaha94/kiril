<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use \Cviebrock\EloquentSluggable\Services\SlugService;

class Category extends Model
{
    use Sluggable;

    public $fillable = ['title'];

    public static function add($title, $slug = null)
    {
        $category = new static;
        $category->title = $title;
        $category->setSlug($slug);
        $category->save();
        return $category;
    }

    public function remove()
    {
        return $this->delete();
    }

    public function setParent($id)
    {
        $this->parent_id = $id;
        $this->save();
    }

    public function setSlug($slug)
    {
        if($slug != null) { $this->slug = $slug; }
    }

    public function sluggable()
    {
        return [
            'slug'  =>  [
                'source'    =>  'title'
            ]
        ];
    }

    public static function getNested()
    {
        $categories = Category::all()->toArray();
        $array = SortService::getNested($categories);

        return $array;
    }

    public static function saveOrder($categories)
    {
        if (count($categories))
        {
            foreach($categories as $order => $category)
            {
                $data = [
                    'parent_id' => (int) $category['parent_id'],
                    'sort' => $order,
                    'depth' => $order,
                    'lft'=>$category['left'],
                    'rgt'=>$category['right'],
                ];
                Category::where('id', $category['id'])
                    -> update($data);
            }
        }
    }
}
