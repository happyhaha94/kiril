<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return view('admin.categories.index', compact(['categories']));
    }

    public function create()
    {
        return view('admin.categories.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, ['title'=>'required']);
        Category::add($request->get('title'));
        return redirect()->route('categories.index');
    }

    public function edit($id)
    {
        $category = Category::find($id);

        return view('admin.categories.edit', ['category'=>$category]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'	=>	'required',
        ]);

        $category = Category::find($id);

        $category->fill($request->all());
        $category->save();

        return redirect()->route('categories.index');
    }

    public function destroy($id)
    {
        Category::find($id)->remove();
        return redirect()->route('categories.index');
    }

    public function show($id)
    {
        $category = Category::find($id);
        return view('admin.categories.show', ['category'=>$category]);
    }

    public function sort()
    {
        return view('admin.categories.sort');
    }

    public function sortAjax(Request $request)
    {
        if ($request->isMethod('post') && $request->get('sortable'))
        {
            Category::saveOrder($request->get('sortable'));
        }

        $categories = Category::getNested();

        return view('admin.categories.sortAjax', ['items'=> $categories]);
    }
}
