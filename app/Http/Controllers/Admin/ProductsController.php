<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    public function index()
    {
        $products = Product::all();

        return view('admin.products.index', compact(['products','categories']));
    }

    public function create()
    {
        $categories = Category::pluck('title','id')->all();
        return view('admin.products.create', compact('categories'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title'=>'required',
            'price'=>'required'
        ]);
        $product = Product::add($request->get('title'), $request->get('price'));

        if($request->has('description'))
        {
            $product->setDescription($request->get('description'));
        }

        if($request->has('category_id'))
        {
            $product->assignCategory($request->get('category_id'));
        }

        if($request->hasFile('image'))
        {
            $product->addCoverImage($request->file('image'));
        }

        return redirect()->route('products.index');
    }

    public function edit($id)
    {
        $product = Product::find($id);
        $categories = Category::pluck('title','id')->all();
        return view('admin.products.edit', ['product'=>$product,'categories'=>$categories]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'=>'required',
            'price'=>'required|numeric'
        ]);
        $product = Product::find($id);
        $product->edit($request->get('title'), $request->get('price'));

        if($request->has('description'))
        {
            $product->setDescription($request->get('description'));
        }

        if($request->has('category_id'))
        {
            $product->assignCategory($request->get('category_id'));
        }

        if($request->hasFile('image'))
        {
            $product->addCoverImage($request->file('image'));
        }

        return redirect()->route('products.index');
    }

    public function destroy($id)
    {
        Product::find($id)->remove();
        return redirect()->route('products.index');
    }

    public function show($id)
    {
        $product = Product::find($id);
        return view('admin.products.show', ['product'=>$product]);
    }

    public function createGallery($id)
    {
        $product = Product::find($id);
        return view('admin.products.gallery', compact('product'));
    }

    public function storeGallery(Request $request, $id)
    {
        $product = Product::find($id);
        if($request->hasFile('images'))
        {
            $product->addGallery($request->file('images'));
        }

        return redirect()->route('createGallery', $id);
    }
}
