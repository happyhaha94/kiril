<?php

namespace App;

use App\ProductImages;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Http\UploadedFile;

class Product extends Model
{
    use Sluggable;
    
    public static function add($title, $price)
    {
        $product = new static;
        $product->title = $title;
        $product->price = $price;
        $product->save();
        return $product;
    }
//    за гаи одна остановка

    public function edit($title, $price)
    {
        $this->title = $title;
        $this->price = $price;
        $this->save();
    }

    public function remove()
    {
        ProductImages::deleteGallery($this->id);
        Image::delete($this->image);
        $this->delete();
    }

    public function setDescription($text)
    {
        $this->description = $text;
        $this->save();
    }

    public function addCoverImage(UploadedFile $image)
    {
        if($this->image != null)
        {
            Image::delete($this->image);
        }
        $this->image = Image::upload($image);
        
        $this->save();
    }

    public function addGallery(Array $images)
    {
        return ProductImages::createGallery($images, $this->id);
    }

    public function assignCategory($categoryID)
    {
        $this->category_id = $categoryID;
        $this->save();
    }

    public function gallery()
    {
        return $this->hasOne('App\ProductImages');
    }
    public function getImagesAttribute($value)
    {
        return $this->gallery ? json_decode($this->gallery->images) : [];
    }

    public function changeTitle($title)
    {
        $this->title = $title;
        $this->save();
    }

    public function setPrice($price)
    {
        $this->price = $price;
        $this->save();
    }

    public function deleteGalleryImage($path)
    {
        ProductImages::deleteImage($path, $this->id);
    }

    public function sluggable()
    {
        return [
            'slug'  =>  [
                'source'    =>  'title'
            ]
        ];
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    public function getCategory()
    {
        return $this->category != null ? $this->category->title : 'Не установлено';
    }

    public function getImage()
    {
        if(is_file($this->image) && file_exists($this->image))
        {
            return $this->image;
        }

        return 'img/no-image.jpg';
    }
}
