<?php

namespace App;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\UploadedFile;

class Image
{
    public static function upload(UploadedFile $image)
    {
        return $image->store('uploads');
    }

    public static function delete($imagePath)
    {
        return (new Filesystem())->delete(public_path($imagePath));
    }
}