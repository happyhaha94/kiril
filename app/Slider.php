<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

class Slider extends Model
{
    protected $table = 'sliders';

    public static function add(UploadedFile $image, $link)
    {
        $slider = new Slider;
        $slider->addImage($image);
        $slider->setLink($link);
        $slider->save();
        return $slider;
    }

    public function remove()
    {
        Image::delete($this->image);
        return $this->delete();
    }

    public function edit(UploadedFile $image, $link)
    {
        $this->addImage($image);
        $this->setLink($link);
        $this->save();
    }

    public function addImage(UploadedFile $image)
    {
        if($this->image != null)
        {
            Image::delete($this->image);
        }
        $this->image = Image::upload($image);
    }

    public function setLink($link)
    {
        $this->link = $link;
    }
}
