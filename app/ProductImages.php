<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImages extends Model
{
    protected $table = 'product_images';

    public static function createGallery(Array $images, $productID)
    {
        $gallery = self::where('product_id',$productID)->first();
        if($gallery == null)
        {
            $gallery = new static;
        }
        
        $gallery->uploadImages($images);
        $gallery->product_id = $productID;
        $gallery->save();
        return $gallery->images;
    }

    public function uploadImages(Array $images)
    {
        $paths = [];
        foreach ($images as $image)
        {
            $paths[] = Image::upload($image);
        }

        if($this->images != null)
        {
            return $this->recordImages(array_merge(json_decode($this->images), $paths));
        }
        return $this->recordImages($paths);
    }

    public static function deleteImage($path, $productID)
    {
        $gallery = self::where('product_id', $productID)->first();
        if($gallery == null) { return;}

        $images = $gallery->remove($path);


        $gallery->save();
    }

    public function remove($path)
    {
        $images = json_decode($this->images);

        foreach($images as $key => $image)
        {
            if($path == $image)
            {
                Image::delete($path);
                unset($images[$key]);
            }
        }

        $this->recordImages($images);
    }

    public function recordImages($paths)
    {
        return $this->images = json_encode($paths);
    }

    public static function deleteGallery($productID)
    {
        $gallery = self::where('product_id', $productID)->first();
        if($gallery != null)
        {
            foreach(json_decode($gallery->images) as $image)
            {
                Image::delete($image);
            }
            $gallery->delete();
        }
    }
}
