<?php

namespace Tests\Feature;

use App\Category;
use App\Product;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductTest extends TestCase
{
    use DatabaseTransactions;

    public function add($title = 'title', $price = 200)
    {
        return Product::add($title, $price);
    }

    /** @test */
    public function createProduct()
    {
        $category = Category::add('category');

        $product = $this->add();
        $product->setDescription('lorem asd');
        $product->addCoverImage(UploadedFile::fake()->image('avatar.jpg'));
        $images = $product->addGallery([
            UploadedFile::fake()->image('avatar.jpg'),
            UploadedFile::fake()->image('avatar.jpg'),
            UploadedFile::fake()->image('avatar.jpg')
        ]);
        $product->assignCategory($category->id);

        $this->assertDatabaseHas('products', [
            'id'    =>  $product->id,
            'title' =>  'title',
            'slug'  =>  'title',
            'price' =>  200,
            'description'   =>  'lorem asd',
            'image' =>  $product->image,
            'category_id'   =>  $product->category_id
        ]);

        $this->assertDatabaseHas('product_images', [
            'images' => $images,
            'product_id'    =>  $product->id
        ]);

        Storage::disk('local')->assertExists($product->image);
        foreach($product->images as $image)
        {
            Storage::disk('local')->assertExists($image);
        }

        (new Filesystem)->cleanDirectory(public_path('uploads'));
    }

    /** @test */
    public function changeTitle()
    {
        $product = $this->add('nice');
        $product->changeTitle('product 2');
        $this->assertDatabaseHas('products',[
           'id' =>  $product->id,
           'title' =>   'product 2'
        ]);
    }

    /** @test */
    public function setPrice()
    {
        $product = $this->add('title', 400);
        $product->setPrice(400);
        $this->assertDatabaseHas('products', [
            'id'    =>  $product->id,
            'price' =>  400
        ]);
    }

    /** @test */
    public function addMoreImagesToGallery()
    {
        $product = $this->add();
        $product->addGallery([
            UploadedFile::fake()->image('avatar.jpg'),
        ]);

        $images = $product->addGallery([
            UploadedFile::fake()->image('avatar.jpg'),
        ]);

        $this->assertDatabaseHas('product_images', [
            'product_id'    =>  $product->id,
            'images'=>$images
        ]);

        foreach(json_decode($images) as $image)
        {
            Storage::disk('local')->assertExists($image);
        }

        (new Filesystem)->cleanDirectory(public_path('uploads'));
    }

    /** @test */
    public function deleteImageFromGallery()
    {
        $product = $this->add();
        $images = $product->addGallery([
            UploadedFile::fake()->image('avatar.jpg'),
        ]);

        $product->deleteGalleryImage($product->images[0]);

        $this->assertDatabaseMissing('product_images', [
            'product_id'    =>  $product->id,
            'images'    =>  $images
        ]);

        $this->assertDatabaseHas('product_images', [
           'product_id' =>  $product->id,
           'images' =>  json_encode([])
        ]);

        Storage::disk('local')->assertMissing($product->images[0]);
    }


    /** @test */
    public function changeCoverImage()
    {
        $product = $this->add();
        $product->addCoverImage(UploadedFile::fake()->image('avatar.jpg'));
        $oldImage = $product->image;

        $product->addCoverImage(UploadedFile::fake()->image('avatar.jpg'));
        $newImage = $product->image;

        Storage::disk('local')->assertMissing($oldImage);

        $this->assertDatabaseHas('products', [
           'id' =>  $product->id,
           'image'  =>  $newImage
        ]);

        $this->assertDatabaseMissing('products',[
            'id'    =>  $product->id,
            'image' =>  $oldImage
        ]);
    }

    /** @test */
    public function removeProduct()
    {
        $category = Category::add('category');

        $product = $this->add();
        $product->setDescription('lorem asd');
        $product->addCoverImage(UploadedFile::fake()->image('avatar.jpg'));
        $images = $product->addGallery([
            UploadedFile::fake()->image('avatar.jpg'),
            UploadedFile::fake()->image('avatar.jpg'),
            UploadedFile::fake()->image('avatar.jpg')
        ]);
        $product->assignCategory($category->id);

        $images = $product->images;

        $product->remove();

        $this->assertDatabaseMissing('products',[
            'id'    =>  $product->id,
        ]);
        $this->assertDatabaseMissing('product_images',[
            'product_id'    =>  $product->id
        ]);

        foreach($images as $image)
        {
            Storage::disk('local')->assertMissing($image);
        }


    }
}
