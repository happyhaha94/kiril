<?php

namespace Tests\Feature;

use App\Slider;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SliderTest extends TestCase
{
    use DatabaseTransactions;

    public function add()
    {
        $image = UploadedFile::fake()->image('avatar.jpg');
        $link = 'www.google.com';
        return Slider::add($image, $link);
    }
    /** @test */
    public function createSlider()
    {
        $slider = $this->add();
        Storage::disk('local')->assertExists($slider->image);
        $this->assertDatabaseHas('sliders', [
            'image' =>  $slider->image,
            'link'  =>  $slider->link
        ]);
        (new Filesystem)->cleanDirectory(public_path('uploads'));
    }
    
    /** @test */
    public function deleteSlider()
    {
        $slider = $this->add();

        $slider->remove();

        Storage::disk('local')->assertMissing($slider->image);
        $this->assertDatabaseMissing('sliders',[
            'image' =>  $slider->image,
            'link'  =>  $slider->link
        ]);

    }

    /** @test */
    public function editSlider()
    {
        $slider = $this->add();

        Storage::disk('local')->assertExists($slider->image);
        $this->assertDatabaseHas('sliders', [
            'image' =>  $slider->image,
            'link'  =>  $slider->link
        ]);


        $newImage = UploadedFile::fake()->image('anotherImage.jpg');
        $newLink = 'www.google.kz';

        $slider->edit($newImage, $newLink);

        $this->assertDatabaseHas('sliders', [
            'link'  =>  $slider->link,
            'image' =>  $slider->image
        ]);
        Storage::disk('local')->assertExists($slider->image);
        (new Filesystem)->cleanDirectory(public_path('uploads'));
    }
}
