<?php

namespace Tests\Feature;

use App\Category;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CategoryTest extends TestCase
{
    use DatabaseTransactions;

    public function add()
    {
        $title = 'привет';
        return Category::add($title);
    }

    /** @test */
    public function createCategory()
    {
        $category = $this->add();

        $this->assertDatabaseHas('categories', [
            'title' =>  $category->title,
            'slug'  =>  $category->slug,
            'parent_id' =>  null
        ]);
    }

    /** @test */
    public function deleteCategory()
    {
        $category = $this->add();
        $category->remove();
        $this->assertDatabaseMissing('categories',[
           'title'  =>  $category->title,
            'slug'  =>  $category->slug
        ]);
    }

    /** @test */
    public function assignParentCategory()
    {
        $parent = $this->add();
        $child = $this->add();

        $child->setParent($parent->id);

        $this->assertDatabaseHas('categories', [
            'title' =>  $child->title,
            'slug'  =>  $child->slug,
            'parent_id' =>  $parent->id
        ]);
    }
    
}
